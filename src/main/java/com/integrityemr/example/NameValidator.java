package com.integrityemr.example;

import java.util.HashSet;
import java.util.Set;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~11/21/16.
 */
public class NameValidator {
    private final Set<String> names;

    public NameValidator(final Set<String> names) {
        this.names = names;
    }

    public Set<String> getNames(final String characters) {
        final Set<String> validNames = new HashSet<>();
        final CharacterMap map = new CharacterMap();
        boolean isCorrect = true;

        for (char character : characters.toCharArray()) {
            map.add(character, (int) characters.toLowerCase().chars().filter(e -> e == character).count());
        }

        for (String name : names) {
            for (char character : name.toCharArray()) {
                if (!map.isValid(character)) {
                    isCorrect = false;
                    break;
                }

                map.incrementTracker(character);
            }

            if (isCorrect) {
                validNames.add(name);
            }

            isCorrect = true;
            map.resetTracker();
        }

        return validNames;
    }
}
